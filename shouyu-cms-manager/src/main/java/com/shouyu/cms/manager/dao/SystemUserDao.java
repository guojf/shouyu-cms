package com.shouyu.cms.manager.dao;


import java.util.List;
import java.util.Map;

import com.shouyu.cms.common.entity.PageModel;
import com.shouyu.cms.manager.dto.UserQueryDTO;
import com.shouyu.cms.manager.entity.User;

public interface SystemUserDao {
	
	public List<User> findUsers(Map<String, Object> params);
	
	/**
	 * 根据用户信息查询分页信息
	 * @param userQueryDTO
	 * @return
	 */
	PageModel<User> queryUserPage(UserQueryDTO userQueryDTO);
	
}
