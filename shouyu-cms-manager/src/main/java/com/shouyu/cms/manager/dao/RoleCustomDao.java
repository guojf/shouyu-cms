package com.shouyu.cms.manager.dao;

import java.util.List;
import java.util.Map;

import com.shouyu.cms.common.entity.PageModel;
import com.shouyu.cms.manager.dto.RoleQueryDTO;
import com.shouyu.cms.manager.entity.Role;

public interface RoleCustomDao {
	
	public List<Role> findRoles(Map<String, Object> params);
	
	/**
	 * 根据查询条件查询角色分页信息
	 * @param userQueryDTO
	 * @return
	 */
	PageModel<Role> queryRolePage(RoleQueryDTO roleQueryDTO);
	
}
