package com.shouyu.cms.manager.dao;


import org.springframework.data.jpa.repository.Query;

import com.shouyu.cms.common.dao.CommonDao;
import com.shouyu.cms.manager.entity.Role;

public interface RoleDao extends RoleCustomDao,CommonDao<Role,String>{

	@Query("from Role r where r.roleName = ?1 ")
	public Role findByRoleName(String roleName);
	
}
