package com.shouyu.cms.manager.dao;

import java.util.List;
import java.util.Map;

import com.shouyu.cms.manager.entity.Resource;


public interface ResourceDaoCustom {

	public List<Resource> findMenuResource(Map<String, Object> params);
	
}
