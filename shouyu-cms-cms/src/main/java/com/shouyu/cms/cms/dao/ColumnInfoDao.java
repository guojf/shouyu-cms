package com.shouyu.cms.cms.dao;

import com.shouyu.cms.cms.entity.ColumnInfo;
import com.shouyu.cms.common.dao.CommonDao;

/**
 * @author xujianfang
 * @desc ColumnInfoDao接口 
 * @date 2017-03-16
 */
public interface ColumnInfoDao extends ColumnInfoDaoCustom,CommonDao<ColumnInfo,String>{

}