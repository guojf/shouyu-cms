package com.shouyu.cms.cms.dao;
import java.util.List;
import java.util.Map;

import com.shouyu.cms.common.entity.PageModel;
import com.shouyu.cms.cms.entity.Article;
import com.shouyu.cms.cms.dto.ArticleQueryDTO;
import com.shouyu.cms.cms.dto.CurrentArticleInfoDTO;

/**
 * @author xujianfang
 * @desc ArticleDaoCustom接口 
 * @date 2017-03-16
 */
public interface ArticleDaoCustom {

      PageModel<Article> queryArticlePage(ArticleQueryDTO articleQueryDTO);

      List<Article> queryArticleList(ArticleQueryDTO articleQueryDTO);

      List<Map<String, Object>> queryStatisMapList(ArticleQueryDTO articleQueryDTO);
      
      List<Article> queryNextArticleList(CurrentArticleInfoDTO currentArticleInfoDTO);
      
      List<Article> queryPreArticleList(CurrentArticleInfoDTO currentArticleInfoDTO);

}