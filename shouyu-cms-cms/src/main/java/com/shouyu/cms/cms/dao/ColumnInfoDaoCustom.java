package com.shouyu.cms.cms.dao;

import com.shouyu.cms.common.entity.PageModel;
import java.util.List;
import com.shouyu.cms.cms.entity.ColumnInfo;
import com.shouyu.cms.cms.dto.ColumnInfoQueryDTO;

/**
 * @author xujianfang
 * @desc ColumnInfoDaoCustom接口 
 * @date 2017-03-16
 */
public interface ColumnInfoDaoCustom {

      PageModel<ColumnInfo> queryColumnInfoPage(ColumnInfoQueryDTO columnInfoQueryDTO);

      List<ColumnInfo> queryColumnInfoList(ColumnInfoQueryDTO columnInfoQueryDTO);



}