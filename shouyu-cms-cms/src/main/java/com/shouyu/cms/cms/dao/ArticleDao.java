package com.shouyu.cms.cms.dao;

import com.shouyu.cms.common.dao.CommonDao;
import com.shouyu.cms.cms.entity.Article;

/**
 * @author xujianfang
 * @desc ArticleDao接口 
 * @date 2017-03-16
 */
public interface ArticleDao extends ArticleDaoCustom, CommonDao<Article,String> {

}