package com.shouyu.cms.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * todo
 *
 * @author 高露 邮箱：<a href="egojit@qq.com">egojit@qq.com</a>
 * @since @datetime
 */
@SpringBootApplication(scanBasePackages = {"com.shouyu.cms"})
@EnableJpaRepositories(basePackages = {"com.shouyu.cms.manager.dao","com.shouyu.cms.cms.dao"})
@EntityScan({"com.shouyu.cms.manager.entity","com.shouyu.cms.cms.entity"})
public class AdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }
}
