<%@ page language="java" pageEncoding="UTF-8"%>

<%@ include file="jstl.jsp" %>

<link href="/static/css/base.css" rel="stylesheet" />
<link href="/static/css/desktop.css" rel="stylesheet" />
<link href="/static/plugins/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" />
<link href="/static/plugins/nice-validator-0.7.3/jquery.validator.css" rel="stylesheet" />
<link rel="shortcut icon" href="/static/favicon/favicon.ico">

<script src="/static/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="/static/plugins/jquery/jquery.cookie.js" type="text/javascript"></script>
<script src="/static/js/common.js"></script>
<script src="/static/js/uiExtend.js"></script>
<script src="/static/js/jquery.nicescroll.min.js"></script>

<script src="/static/plugins/layer/layer.js"></script>
<script type="text/javascript" src="/static/plugins/laypage/laypage.js"></script>
<script src="/static/plugins/nice-validator-0.7.3/src/jquery.validator.js" type="text/javascript"></script>
<script src="/static/plugins/nice-validator-0.7.3/local/zh_CN.js" type="text/javascript"></script>
<script src="/static/plugins/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/static/plugins/jquery/jquery.form.js" type="text/javascript"></script>
<script src="/static/plugins/jquery/jquery.formatDateTime.js" type="text/javascript"></script>
<script src="/static/plugins/base.js" type="text/javascript"></script>
