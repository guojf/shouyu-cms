package com.shouyu.cms.web.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * todo
 *
 * @author 高露 邮箱：<a href="egojit@qq.com">egojit@qq.com</a>
 * @since @datetime
 */
@SpringBootApplication(scanBasePackages = {"com.shouyu.cms"})
@EnableJpaRepositories(basePackages = {"com.shouyu.cms.cms.dao"})
@EntityScan({"com.shouyu.cms.cms.entity"})
public class WebApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }
}
