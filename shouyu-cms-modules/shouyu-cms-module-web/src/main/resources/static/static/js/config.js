﻿	// window.serverUploadPath = window.server +"/";
	window.notImgUrl = "/static/images/img_null.png";
	jc.require.url.push( "/static/js/jquery.pagination.js");
	jc.require.url.push("/static/js/jquery.nicescroll.min.js");
	jc.require.url.push("/static/css/common.css");
	jc.require.url.push("/static/js/uiExtend.js");
	
	jc.require.success = function () {
	    if ($("html").niceScroll) {
	        $("html").niceScroll({ zindex: 9999, autohidemode: false, cursorwidth: "4px", cursorcolor: "#333", cursorborder: 0, cursoropacitymax: 0.8 });
	    }
	}
	
	window.routerList = {
	    index: "/index.html",
	    menuAndTextlist: "/menuAndTextlist/menuAndTextlist.html",
	    menuAndDetail: "/menuAndDetail/menuAndDetail.html"
	}
	
	
	window.path = {
	    cmsApiColumnList: "/cms/api/column/list",
	    cmsApiArticleList: "/cms/api/article/list",
	    cmsApiArticleDetail: "/cms/api/article/detail",
	    cmsApiArticleNext: "/cms/api/article/next",
	    cmsApiArticlePre:  "/cms/api/article/pre"
	}
	
	window.arraySortASC = function (array, keyName) {
	    if (!array) return array;
	    array.sort(function (num1, num2) {
	        return parseInt(num1[keyName]) - parseInt(num2[keyName]);
	    });
	    return array;
	}
	
	
	window.resource = function (path, data, fnSuccess, async) {
	    var _data = data;
	    if (async == undefined) {
	        async = true;
	    }
	    $.ajax({
	        url: window.path[path],
	        data: data,
	        success: function (res) {
	            if (!fnSuccess) return false;
	            if (res.success) {
	                var data = res.data;
	                fnSuccess(data);
	                /*if (isLocalhost) {
	                    console.log("%c isSuccessTrue : " + window.path[path] + "?" + jc.param.stringify(_data), "color: green", data);
	                }
	                else {
	                    console.log("%c isSuccessFail : " + window.path[path] + "?" + jc.param.stringify(_data), "color: red", res.msg);
	                }*/
	            }
	            else {
	                alert("isNull : " + window.path[path] + "?" + jc.param.stringify(_data));
	            }
	        },
	        error: function (e) {
	        	debugger
	            console.log("%c isError : " + window.path[path] + "?" + jc.param.stringify(_data), "color: red", res.msg);
	        },
	        async: async
	    });
	}
	
	window.router = function (routerListName, param, only) {
	    var nowParam = jc.param.getObject();
	    var resultParam = "";
	    if (!routerListName) {
	        routerListName = "index";
	    }
	    if (only) {
	        nowParam = param;
	    }
	    else {
	        for (var attr in param) {
	            nowParam[attr] = param[attr];
	        }
	    }
	    resultParam = jc.param.stringify(nowParam);
	    if (resultParam) {
	        resultParam = "?" + resultParam;
	    }
	    if (routerListName == "index") {
	        resultParam = "";
	    }
	    window.location.href = window.routerList[routerListName] + resultParam;
	}
